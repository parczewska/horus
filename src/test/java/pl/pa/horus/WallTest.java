package pl.pa.horus;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class WallTest {
    @Test
    public void shouldFindByColorIfColorExist() {
        //given
        Wall wall = new Wall();
        BlockTest blockTest1 = new BlockTest("zielony", "szkło");
        wall.add(blockTest1);
        BlockTest blockTest2 = new BlockTest("czerwony", "kamień");
        wall.add(blockTest2);
        CompositeBlockTest compositeBlockTest = new CompositeBlockTest("szary", "papier", List.of(blockTest1, blockTest2));
        wall.add(compositeBlockTest);
        //when
        Optional<Block> result = wall.findBlockByColor("szary");
        //then
        Assertions.assertTrue(result.isPresent());
    }

    @Test
    public void shouldNotFindByColorIfColorNotExist() {
        //given
        Wall wall = new Wall();
        BlockTest blockTest1 = new BlockTest("żółty", "plastik");
        wall.add(blockTest1);
        BlockTest blockTest2 = new BlockTest("czerwony", "szkło");
        wall.add(blockTest1);
        CompositeBlockTest compositeBlockTest = new CompositeBlockTest("fioletowy", "szkło", List.of(blockTest1, blockTest2));
        wall.add(compositeBlockTest);
        //when
        Optional<Block> result = wall.findBlockByColor("czarny");
        //then
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    public void shouldFindByMaterialIfMaterialExist() {
        //given
        Wall wall = new Wall();
        BlockTest blockTest1 = new BlockTest("zielony", "szkło");
        wall.add(blockTest1);
        BlockTest blockTest2 = new BlockTest("czerwony", "kamień");
        wall.add(blockTest2);
        CompositeBlockTest compositeBlockTest = new CompositeBlockTest("czarny", "papier", List.of(blockTest1, blockTest2));
        wall.add(compositeBlockTest);
        //when
        List<Block> result = wall.findBlockByMaterial("szkło");
        //then
        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    public void shouldNotFindByMaterialIfMaterialNotExist() {
        //given
        Wall wall = new Wall();
        BlockTest blockTest1 = new BlockTest("żółty", "plastik");
        wall.add(blockTest1);
        BlockTest blockTest2 = new BlockTest("czerwony", "szkło");
        wall.add(blockTest2);
        CompositeBlockTest compositeBlockTest = new CompositeBlockTest("czerwony", "plastik", List.of(blockTest1, blockTest2));
        wall.add(compositeBlockTest);
        //when
        List<Block> result = wall.findBlockByMaterial("kamień");
        //then
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void shouldCount() {
        //given
        Wall wall = new Wall();
        BlockTest blockTest1 = new BlockTest("żółty", "plastik");
        wall.add(blockTest1);
        BlockTest blockTest2 = new BlockTest("niebieski", "szkło");
        wall.add(blockTest2);
        BlockTest blockTest3 = new BlockTest("zielony", "szkło");
        wall.add(blockTest3);
        CompositeBlockTest compositeBlockTest1 = new CompositeBlockTest("szary", "papier", List.of(blockTest1, blockTest2, blockTest3));
        wall.add(compositeBlockTest1);
        CompositeBlockTest compositeBlockTest2 = new CompositeBlockTest("szary", "papier", List.of(blockTest1, blockTest2, blockTest3));
        wall.add(compositeBlockTest2);
        //when
        int result = wall.count();
        //then
        Assertions.assertEquals(9, result);
    }
}
