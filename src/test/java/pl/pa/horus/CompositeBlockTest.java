package pl.pa.horus;

import java.util.List;

public class CompositeBlockTest implements CompositeBlock {

    private String color;
    private String material;
    private List<Block> blocks;

    CompositeBlockTest(String color, String material, List<Block> blocks) {
        this.color = color;
        this.material = material;
        this.blocks = blocks;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMaterial() {
        return material;
    }

    @Override
    public List<Block> getBlocks() {
        return blocks;
    }
}
