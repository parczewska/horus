package pl.pa.horus;

public interface Block {

    String getColor();
    String getMaterial();
}
