package pl.pa.horus;

import java.util.List;

public interface CompositeBlock extends Block{
    List<Block> getBlocks();
}
