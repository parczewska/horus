package pl.pa.horus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Wall implements Structure {

    private List<Block> blocks;

    public Wall() {
        this.blocks = new ArrayList<>();
    }

    public void add(Block block) {
        blocks.add(block);
    }

    // zwraca dowolny element o podanym kolorze
    @Override
    public Optional<Block> findBlockByColor(String color) {

        return blocks.stream().filter(e -> e.getColor().equals(color)).findFirst();
    }

    // zwraca wszystkie elementy z danego materiału
    @Override
    public List<Block> findBlockByMaterial(String material) {
        return blocks.stream().filter(e -> e.getMaterial().equals(material)).collect(Collectors.toList());
    }

    @Override
    public int count() {
        long compositeBlocksSize = blocks.stream().filter(CompositeBlock.class::isInstance)
                .map(e -> ((CompositeBlock) e).getBlocks())
                .mapToInt(List::size).sum();
        return (int) (compositeBlocksSize + (blocks.stream().filter(e -> !(e instanceof CompositeBlock)).count()));
    }
}
